//
//  InsightViewController.m
//  Strategy Vault
//
//  Created by Ismael Zavala on 10/20/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import "InsightViewController.h"

@interface InsightViewController ()

@end

@implementation InsightViewController
{
    AVAudioRecorder *recorder;
    AVAudioPlayer *player;
    NSArray *_pickerData;
    NSString *category;
    NSString *categoryName;
    BOOL buttonFlashing;
    BOOL recording;
    BOOL playing;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background-slice-"]];
    
    // Initialize Data of picker
    // Initialize Data
    _pickerData = @[ @[@"tag-grey", @"No Tag"],
                     @[@"tag-green", @"Market"],
                     @[@"tag-red", @"Customers"],
                     @[@"tag-purple", @"Competitors"],
                     @[@"tag-blue", @"Company"],
                     @[@"tag-pink", @"Individual"],
                     @[@"tag-orange", @"General"]];
    
    
    
    //connect data
    self.picker.dataSource = self;
    self.picker.delegate = self;
    category = @"tag-grey.png";
    categoryName = @"No Tag";
    
    
    
    self.titleField.layer.borderWidth = 1.0f;
    self.titleField.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
    self.titleField.leftView = paddingView;
    self.titleField.leftViewMode = UITextFieldViewModeAlways;
    self.titleField.backgroundColor = [UIColor clearColor];
    
    [self.stopButton setBackgroundImage:[Utilities imageFromColor:[UIColor darkGrayColor]]
                               forState:UIControlStateDisabled];
    [self.recordButton setBackgroundImage:[Utilities imageFromColor:[UIColor darkGrayColor]]
                                 forState:UIControlStateDisabled];
    
    [self.stopButton setEnabled:NO];
    
    if (!$safe(self.insight)) {
        self.insight = [[NSMutableDictionary alloc] init];
        NSInteger time = [[NSDate date] timeIntervalSince1970];
        self.insight[@"file"] = $str(@"%ld.m4a", (long)time);
    }
    
    self.titleField.text = self.insight[@"title"];
    self.textView.text = self.insight[@"text"];
    
    
    // Set the audio file
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               self.insight[@"file"],
                               nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    // Setup audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    
    // Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    // Initiate and prepare the recorder
    recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:NULL];
    recorder.delegate = self;
    recorder.meteringEnabled = YES;
    
    recording = NO;
    playing = NO;
    [player setVolume:1.0];
    
    NSError *error;
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:(AVAudioSessionCategoryOptionDefaultToSpeaker | AVAudioSessionCategoryOptionAllowBluetooth) error:&error];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillAppear:animated];
    [self.navigationController setToolbarHidden:YES];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style: UIBarButtonItemStyleBordered target:self action:@selector(Back)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    self.scrollView.contentSize = CGSizeMake(320, 568);
    
    CGRect fullScreenRect = [[UIScreen mainScreen] bounds];
    if (fullScreenRect.size.height > 500) {
        self.scrollView.scrollEnabled = NO;
    }
    
    [self.picker selectRow:1 inComponent:0 animated:YES];
}

- (IBAction)Back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)keyboardOnScreen:(NSNotification *)notification
{
    _savebutton.title = @"Done";
}


- (void)keyboardOffScreen:(NSNotification *)notification
{
    _savebutton.title = @"Save";
}

- (IBAction)saveButtonTapped:(id)sender
{
    if (!([_textView isFirstResponder] || [_titleField isFirstResponder]))
    {
    
        NSDate *today = [NSDate date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        NSString *dateString = [dateFormat stringFromDate:today];
    
        self.insight[@"title"] = self.titleField.text;
        self.insight[@"text"] = self.textView.text;
        self.insight[@"date"] = dateString;
        self.insight[@"tag"] = category;
        self.insight[@"categoryName"] = categoryName;
    
        if (self.index >= 0) {
            [self.parent saveInsight:self.insight atIndex:self.index];
        }
        else {
            [self.parent saveInsight:self.insight atIndex:-1];
            }
    
        [self.navigationController popViewControllerAnimated:YES];
    } else{
            [_textView resignFirstResponder];
            [_titleField resignFirstResponder];
        }
    
}

#pragma mark - Voice Recorder

- (IBAction)recordPauseTapped:(id)sender
{
    [recorder prepareToRecord];
    
    // Stop the audio player before recording
    if (player.playing) {
        [player stop];
        [self.playButton setEnabled:NO];
    }
    
    if (!recorder.recording) {
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setActive:YES error:nil];
        
        // Start recording
        [recorder record];
        [self.recordButton setTitle:@"Pause" forState:UIControlStateNormal];
        recording = YES;
        [self startFlashingbutton];
        
    }
    else {
        
        // Pause recording
        [recorder pause];
        [self.recordButton setTitle:@"Record" forState:UIControlStateNormal];
        recording = NO;
        [self stopFlashingbutton];
        [self.playButton setEnabled:NO];
        [self.playButton setBackgroundColor:[UIColor darkGrayColor]];
    }
    
    [self.stopButton setEnabled:YES];
    [self.playButton setEnabled:NO];
}

- (IBAction)stopTapped:(id)sender
{
    [recorder stop];
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
    
    recording = NO;
    [self stopFlashingbutton];
    [self.playButton setEnabled:YES];
    [self.playButton setBackgroundColor:[UIColor colorWithRed:0.749 green:0.4 blue:0.161 alpha:1]];
    
    
    if (playing ) {
        [player stop];
        playing = NO;
        
        [self.stopButton setEnabled:NO];
        [self.playButton setEnabled:YES];
        [self.recordButton setEnabled:YES];
    }
}

- (IBAction)playTapped:(id)sender
{
    if (!recorder.recording) {
        [self.recordButton setEnabled:NO];
        [self.playButton setEnabled:NO];
        [self.stopButton setEnabled:YES];
        
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:recorder.url error:nil];
        [player setDelegate:self];
        [player play];
        
        [self.playButton setEnabled:NO];
        [self.playButton setBackgroundColor:[UIColor darkGrayColor]];
        
        recording = NO;
        playing = YES;
    }
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [self.playButton setEnabled:YES];
    [self.recordButton setEnabled:YES];
    [self.stopButton setEnabled:NO];
    
    [self.playButton setBackgroundColor:[UIColor colorWithRed:0.749 green:0.4 blue:0.161 alpha:1]];
}

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag
{
    [self.recordButton setTitle:@"Record" forState:UIControlStateNormal];
    
    [self.stopButton setEnabled:NO];
    [self.playButton setEnabled:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) stopFlashingbutton
{
    if (!buttonFlashing) return;
    buttonFlashing = NO;
    [self.playButton setTitle:@"Play" forState:UIControlStateNormal];
    [self.playButton setBackgroundColor:[UIColor colorWithRed:0.749 green:0.4 blue:0.161 alpha:1]];
    
    
    [UIView animateWithDuration:0.8
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut |
     UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.playButton.alpha = 1.0f;
                     }
                     completion:^(BOOL finished){
                         // Do nothing
                     }];
}

- (BOOL)overrideOutputAudioPort:(AVAudioSessionPortOverride)portOverride
                          error:(NSError **)outError
{
    
    
    return YES;
}


#pragma mark - UIPicker

// The number of columns of data
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _pickerData.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return _pickerData[row][component];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row
          forComponent:(NSInteger)component reusingView:(UIView *)view
{
    NSArray *keysArray = [NSArray arrayWithObjects: @"Image", @"LabelText", nil];
    NSDictionary *dict = [[NSDictionary alloc] initWithObjects: [_pickerData objectAtIndex:row]
                                                       forKeys: keysArray];
    
    UIImageView *image = [[UIImageView alloc]initWithImage:[UIImage imageNamed:[dict valueForKey:@"Image"]]];
    image.frame = CGRectMake(-100, 0, 110, 40);
    
    UILabel *channelLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, -15, 100, 60)];
    channelLabel.text = [dict valueForKey:@"LabelText"];
    channelLabel.backgroundColor = [UIColor clearColor];
    
    
    UIView *tmpView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 80, 40)];
    [tmpView insertSubview:image atIndex:1];
    [tmpView insertSubview:channelLabel atIndex:0];
    
    return tmpView;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView
rowHeightForComponent:(NSInteger)component
{
    
    return 45;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row   inComponent:(NSInteger)component{
        
    switch(row)
    {
        case 0:
            NSLog(@"case 0");
            category = @"tag-grey.png";
            categoryName = @"No Tag";
            break;
            
        case 1:
            NSLog(@"case 1");
            category = @"tag-green.png";
            categoryName = @"Market";
            break;
            
        case 2:
            NSLog(@"case 2");
            category = @"tag-red.png";
            categoryName = @"Customers";
            break;
            
        case 3:
            NSLog(@"case 3");
            category = @"tag-purple.png";
            categoryName = @"Competitors";
            break;
            
        case 4:
            NSLog(@"case 4");
            category = @"tag-blue.png";
            categoryName = @"Company";
            break;
            
        case 5:
            NSLog(@"case 5");
            category = @"tag-pink.png";
            categoryName = @"Individual";
            break;
            
        case 6:
            NSLog(@"case 6");
            category = @"tag-orange.png";
            categoryName = @"General";
            break;
    }
}

-(void) startFlashingbutton
{
    if (buttonFlashing) return;
    buttonFlashing = YES;
    
    [self.playButton setBackgroundColor:[UIColor redColor]];
    [self.playButton setTitle:@"Recording" forState:UIControlStateNormal];

    
    self.playButton.alpha = 1.0f;
    [UIView animateWithDuration:0.8
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut |
     UIViewAnimationOptionRepeat |
     UIViewAnimationOptionAutoreverse |
     UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         self.playButton.alpha = 0.0f;
                     }
                     completion:^(BOOL finished){
                         // Do nothing
                     }];
}

@end

