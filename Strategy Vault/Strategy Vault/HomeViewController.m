//
//  HomeViewController.m
//  Strategy Vault
//
//  Created by Chris Anderson on 9/4/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import "HomeViewController.h"
#import "WebViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _homeNavigationImage.image = [UIImage imageNamed: @"home-header-.png"];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }

}

//hides the navigation bar
- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
    [self.navigationController setToolbarHidden:YES];
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        [self performSegueWithIdentifier:@"insight" sender:self];
    }
    else if (indexPath.row == 1) {
        [self performSegueWithIdentifier:@"thought" sender:self];
    }
    else if (indexPath.row == 2) {
        [self performSegueWithIdentifier:@"library" sender:self];
    }
    else if (indexPath.row == 3) {
        [self performSegueWithIdentifier:@"glossary1" sender:self];
    }
    else if (indexPath.row == 4) {
        [self performSegueWithIdentifier:@"strategy" sender:self];
    }
    else if (indexPath.row == 5) {
        [self performSegueWithIdentifier:@"behavioral" sender:self];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView
                             dequeueReusableCellWithIdentifier:@"categories"];
    
    UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"blank-box-.png"]];
    
    cell.backgroundView = backgroundView;
    
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    
    UILabel *label = (UILabel *) [cell viewWithTag: 100];
    
    if (indexPath.row == 0)
    {
        label.text = @"INSIGHTS";
    } else if ( indexPath.row == 1)
    {
        label.text = @"THOUGHT OF THE DAY";
    } else if ( indexPath.row == 2)
    {
        label.text = @"KNOWLEDGE";
    } else if ( indexPath.row == 3)
    {
        label.text = @"GLOSSARY";
    } else if ( indexPath.row == 4)
    {
        label.text = @"STRATEGY PROCESS";
    } else if ( indexPath.row == 5)
    {
        label.text = @"THINKING TRAPS";
    }
    
    return cell;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"instructions"]) {
        NSLog(@"HERE");
        WebViewController *vc = segue.destinationViewController;
        vc.url = @"instructions";
        [vc.webView reload];
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
