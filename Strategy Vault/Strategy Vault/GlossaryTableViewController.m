//
//  GlossaryTableViewController.m
//  Strategy Vault
//
//  Created by Chris Anderson on 9/4/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import "GlossaryTableViewController.h"
#import "GlossaryDetailViewController.h"

@interface GlossaryTableViewController ()

@end

@implementation GlossaryTableViewController
{
    NSArray *items;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    items = [Utilities jsonArrayFromFile:@"glossary"];
    
    [self.tableView setBackgroundView:
     [[UIImageView alloc] initWithImage:
      [UIImage imageNamed:@"background-slice-"]]];

}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillAppear:animated];
    [self.navigationController setToolbarHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [items count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"glossaryCell" forIndexPath:indexPath];
    
    cell.textLabel.text = [[items objectAtIndex:indexPath.row] objectForKey:@"title"];
    
    UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"steel-bar-.png"]];
    
    
    cell.backgroundView = backgroundView;
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"glossaryDetail" sender:indexPath];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = sender;
    NSDictionary *item = [items objectAtIndex:indexPath.row];
    GlossaryDetailViewController *vc = segue.destinationViewController;
    vc.title = @"Definition";
    vc.item = item;
}


@end
