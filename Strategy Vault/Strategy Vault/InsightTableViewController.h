//
//  InsightTableViewController.h
//  Strategy Vault
//
//  Created by Chris Anderson on 9/9/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InsightTableViewController : UITableViewController

@property (nonatomic, strong) UIBarButtonItem *create;

- (IBAction)createButtonTapped:(id)sender;
- (void)saveInsight:(NSDictionary *)insight atIndex:(NSInteger)index;
- (IBAction)dateBarButtonPressed:(id)sender;
- (IBAction)TagBarButtonPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UITabBar *tabBar;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *DateBarButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *TagBarButton;
@property (weak, nonatomic) IBOutlet UIImageView *steelBarImage;

@end
