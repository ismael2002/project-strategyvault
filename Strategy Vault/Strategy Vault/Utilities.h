//
//  Utilities.h
//  Strategy Vault
//
//  Created by Chris Anderson on 9/2/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utilities : NSObject

+ (NSArray *)jsonArrayFromFile:(NSString *)filename;
+ (void)jsonToFile:(NSArray *)json filename:(NSString *)filename;
+ (UIImage *)imageFromColor:(UIColor *)color;

@end
