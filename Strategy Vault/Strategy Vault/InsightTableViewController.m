//
//  InsightTableViewController.m
//  Strategy Vault
//
//  Created by Chris Anderson on 9/9/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import "InsightTableViewController.h"
#import "InsightViewController.h"


@implementation InsightTableViewController
{
    NSMutableArray *insights;
    NSInteger unNamedInsight;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    CGRect labelFrame = CGRectMake(10, 0, 310, 100);
    UILabel *labelView = [[UILabel alloc] initWithFrame:labelFrame];
    labelView.text = @"Click the + sign to add an insight using text or voice.\nTo delete, swipe right to left.";
    labelView.numberOfLines = 0;
    labelView.textAlignment = NSTextAlignmentCenter;

    [self.steelBarImage addSubview:labelView];

    @try {
        insights = [[Utilities jsonArrayFromFile:@"insights"] mutableCopy];
    }
    @catch (NSException *exception) {
        insights = [[NSMutableArray alloc] init];
    }
    
    [self.tableView setBackgroundView:
     [[UIImageView alloc] initWithImage:
      [UIImage imageNamed:@"background-slice-"]]];
   
}

- (void)saveInsight:(NSDictionary *)insight atIndex:(NSInteger)index
{
    NSLog(@"%ld", (long)index);
    if (index >= 0) {
        insights[index] = insight;
    }
    else {
        [insights addObject:insight];
    }
    [Utilities jsonToFile:insights filename:@"insights"];
    [self.tableView reloadData];
}

- (IBAction)dateBarButtonPressed:(id)sender
{
    NSSortDescriptor *firstDescriptor =
    [[NSSortDescriptor alloc] initWithKey:@"date"
                                ascending:NO
                                 selector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSSortDescriptor *secondDescriptor =
    [[NSSortDescriptor alloc] initWithKey:@"tag"
                                ascending:YES
                                 selector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSSortDescriptor *lastDescriptor =
    [[NSSortDescriptor alloc] initWithKey:@"title"
                                ascending:YES
                                 selector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSArray *descriptors = [NSArray arrayWithObjects:firstDescriptor, secondDescriptor, lastDescriptor, nil];
    NSArray *sortedArray = [insights sortedArrayUsingDescriptors:descriptors];
    
    [Utilities jsonToFile:sortedArray filename:@"insights"];
    [self.tableView reloadData];
    [self viewDidLoad];
}

- (IBAction)TagBarButtonPressed:(id)sender
{
    
    NSSortDescriptor *firstDescriptor =
    [[NSSortDescriptor alloc] initWithKey:@"tag"
                                ascending:YES
                                 selector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSSortDescriptor *lastDescriptor =
    [[NSSortDescriptor alloc] initWithKey:@"title"
                                ascending:YES
                                 selector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSArray *descriptors = [NSArray arrayWithObjects:firstDescriptor, lastDescriptor, nil];
    NSArray *sortedArray = [insights sortedArrayUsingDescriptors:descriptors];
    
    [Utilities jsonToFile:sortedArray filename:@"insights"];
    [self.tableView reloadData];
    [self viewDidLoad];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillAppear:animated];
    
    [self.navigationController setToolbarHidden:NO];
    [self.navigationController.toolbar setBarStyle:UIBarStyleBlack];
    
}

- (IBAction)createButtonTapped:(id)sender
{
    [self performSegueWithIdentifier:@"insightDetail" sender:false];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//deletes rows from insight table view
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [insights removeObjectAtIndex:indexPath.row];
    NSArray *indexPaths = @[indexPath];
    
    [tableView deleteRowsAtIndexPaths:indexPaths
                     withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [Utilities jsonToFile:insights filename:@"insights"];
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [insights count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"insightCell" forIndexPath:indexPath];
    
    UILabel *labelTitle = (UILabel *) [cell viewWithTag: 300];
    UILabel *labelDate = (UILabel *) [cell viewWithTag: 301];
    UIImageView *cellImage = (UIImageView *) [cell viewWithTag:302];
    UILabel *category = (UILabel *) [cell viewWithTag:303];
    
    
    NSString *tag =[[insights objectAtIndex:indexPath.row] objectForKey:@"tag"];
    cellImage.image = [UIImage imageNamed:tag];
    
    
    labelDate.text =[[insights objectAtIndex:indexPath.row] objectForKey:@"date"];
    NSString *title = [[insights objectAtIndex:indexPath.row] objectForKey:@"title"];
    category.text = [[insights objectAtIndex:indexPath.row] objectForKey:@"categoryName"];
    
    if ([title isEqualToString:@""]) {
        title = [NSString stringWithFormat: @"Insight"];
        labelTitle.text = title;
        
        [insights [indexPath.row] setObject:title forKey:@"title"];
    } else
    {
        labelTitle.text = title;
    }
    
    UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"steel-bar-.png"]];
    cell.backgroundView = backgroundView;
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"insightDetail" sender:indexPath];
    
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    InsightViewController *insight = segue.destinationViewController;
    if (sender) {
        NSIndexPath *indexPath = sender;
        insight.insight = [[insights objectAtIndex:indexPath.row] mutableCopy];
        insight.index = indexPath.row;
        
    } else {
        insight.index = -1;
    }
    insight.parent = self;
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end

