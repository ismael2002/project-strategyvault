//
//  BehaviorChecklistTableViewController.m
//  Strategy Vault
//
//  Created by Chris Anderson on 9/4/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import "BehaviorChecklistTableViewController.h"

#define FONT_SIZE 14.0f
#define CELL_CONTENT_WIDTH 320.0f
#define CELL_CONTENT_MARGIN 10.0f

@interface BehaviorChecklistTableViewController ()
{

}

@end

@implementation BehaviorChecklistTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.titleLabel.text = [self.behaviors objectForKey:@"title"];
    self.intro.text = [self.behaviors objectForKey:@"intro"];
    self.summary.text = [self.behaviors objectForKey:@"summary"];
    
    self.tableView.allowsMultipleSelection = YES;
    
    [self.tableView setBackgroundView:
    [[UIImageView alloc] initWithImage:
    [UIImage imageNamed:@"background-slice-"]]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [[self.behaviors objectForKey:@"items"] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"behaviorCheckCell" forIndexPath:indexPath];
    
    NSDictionary *item = [[self.behaviors objectForKey:@"items"] objectAtIndex:indexPath.row];
    
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.text = [item objectForKey:@"title"];
    
    UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"steel-bar-.png"]];
    
    cell.backgroundView = backgroundView;
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    if ([[item objectForKey:@"checked"] boolValue]) {
        cell.imageView.image = [UIImage imageNamed:@"checkbox-checked-568.png"];
        NSLog(@"checked box");
    }
    else {
        cell.imageView.image = [UIImage imageNamed:@"checkbox-unchecked-568.png"];
        NSLog(@"unchecked box placed");
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSDictionary *item = [[self.behaviors objectForKey:@"items"] objectAtIndex:indexPath.row];
    NSString *text = [item objectForKey:@"title"];
    
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2) - 10, 20000.0f);
    
    if ([[item objectForKey:@"checked"] boolValue]) {
        constraint = CGSizeMake(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2) - 30, 20000.0f);
    }
    
    UIFont *font = [UIFont boldSystemFontOfSize:18];
    
    CGRect textRect = [text boundingRectWithSize:constraint
                                             options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                          attributes:@{NSFontAttributeName:font}
                                             context:nil];
    
    CGFloat height = MAX(textRect.size.height, 44.0f);
    
    return height + (CELL_CONTENT_MARGIN * 2) + 15;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableDictionary *dic = [[[self.behaviors objectForKey:@"items"] objectAtIndex:indexPath.row] mutableCopy];
    
    if ([dic[@"checked"] boolValue]) {
        dic[@"checked"] = @NO;
        [tableView cellForRowAtIndexPath:indexPath].imageView.image = [UIImage imageNamed:@"checkbox-unchecked-568.png"];
    }
    else {
        dic[@"checked"] = @YES;
        [tableView cellForRowAtIndexPath:indexPath].imageView.image = [UIImage imageNamed:@"checkbox-checked-568.png"];
    }
    
    
    NSIndexPath *path = [NSIndexPath indexPathForRow:indexPath.row inSection:self.section];
    [self.parent saveBehavior:dic
                      atIndex:path];

}


- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *dic = [[[self.behaviors objectForKey:@"items"] objectAtIndex:indexPath.row] mutableCopy];
    
    if ([dic[@"checked"] boolValue]) {
        dic[@"checked"] = @NO;
        [tableView cellForRowAtIndexPath:indexPath].imageView.image = [UIImage imageNamed:@"checkbox-unchecked-568.png"];
    }
    else {
        dic[@"checked"] = @YES;
        [tableView cellForRowAtIndexPath:indexPath].imageView.image = [UIImage imageNamed:@"checkbox-checked-568.png"];
    }
    
    
    NSIndexPath *path = [NSIndexPath indexPathForRow:indexPath.row inSection:self.section];
    [self.parent saveBehavior:dic
                      atIndex:path];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
