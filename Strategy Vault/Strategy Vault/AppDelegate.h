//
//  AppDelegate.h
//  Strategy Vault
//
//  Created by Chris Anderson on 9/2/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

-(void) custumomizeAppearance;

@end
