//
//  RequestInviteViewController.m
//  Strategy Vault
//
//  Created by Chris Anderson on 8/28/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import "RequestInviteViewController.h"

@interface RequestInviteViewController ()

@end

@implementation RequestInviteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.password setReturnKeyType:UIReturnKeyDone];
    [self.email setKeyboardType:UIKeyboardTypeEmailAddress];
    // Do any additional setup after loading the view.
}

- (IBAction)submit:(id)sender
{
    NSArray *richEmail = [NSArray arrayWithObjects: @"rich@strategyskills.com", nil];
    
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    [controller setToRecipients:richEmail];
    [controller setSubject:@"Strategy Vault Code Request"];
    [controller setMessageBody:$str(@"I would like to request a code for Strategy Vault.\n\nFirst Name: %@\nLast Name: %@\nEmail: %@\n Password: %@",
                                    self.firstname.text,
                                    self.lastname.text,
                                    self.email.text,
                                    self.password.text) isHTML:NO];
    if (controller) {
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    
    if (theTextField == self.firstname) {
        [theTextField resignFirstResponder];
        [self.lastname becomeFirstResponder];
    }
    
    else if (theTextField == self.lastname) {
        [theTextField resignFirstResponder];
        [self.email becomeFirstResponder];
    }
    
    else if (theTextField == self.email) {
        [theTextField resignFirstResponder];
        [self.password setReturnKeyType:UIReturnKeyDone];
        [self.password becomeFirstResponder];
    }
    
    else if (theTextField == self.password) {
        [self.submit sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
    
    return YES;
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
