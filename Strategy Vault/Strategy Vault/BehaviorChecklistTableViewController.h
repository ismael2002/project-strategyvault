//
//  BehaviorChecklistTableViewController.h
//  Strategy Vault
//
//  Created by Chris Anderson on 9/4/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BehavioralTableViewController.h"

@interface BehaviorChecklistTableViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *intro;
@property (weak, nonatomic) IBOutlet UILabel *summary;


@property NSDictionary *behaviors;
@property NSInteger section;
@property BehavioralTableViewController *parent;

@end
