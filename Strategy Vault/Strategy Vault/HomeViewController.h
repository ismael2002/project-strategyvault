//
//  HomeViewController.h
//  Strategy Vault
//
//  Created by Chris Anderson on 9/4/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UITableViewController


@property (strong, nonatomic) IBOutlet UIView *block1;
@property (strong, nonatomic) IBOutlet UIView *block2;
@property (strong, nonatomic) IBOutlet UIView *block3;
@property (strong, nonatomic) IBOutlet UIView *block4;
@property (strong, nonatomic) IBOutlet UIView *block5;
@property (strong, nonatomic) IBOutlet UIView *block6;
@property (strong, nonatomic) IBOutlet UIView *block7;
@property (weak, nonatomic) IBOutlet UIImageView *homeNavigationImage;

@end
