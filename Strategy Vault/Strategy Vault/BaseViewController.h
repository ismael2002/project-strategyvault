//
//  BaseViewController.h
//  Strategy Vault
//
//  Created by Chris Anderson on 8/2/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController <UITextFieldDelegate>

@end
