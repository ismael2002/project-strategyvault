//
//  RequestInviteViewController.h
//  Strategy Vault
//
//  Created by Chris Anderson on 8/28/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import "BaseViewController.h"
#import <MessageUI/MFMailComposeViewController.h>

@interface RequestInviteViewController : BaseViewController <MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UITextField *firstname;
@property (strong, nonatomic) IBOutlet UITextField *lastname;
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *password;

@property (weak, nonatomic) IBOutlet UIButton *submit;

- (IBAction)submit:(id)sender;


@end
