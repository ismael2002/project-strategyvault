<style>
    .article {
        padding: 15px;
        font-family: "Helvetica";
    }
</style>
<article class="article">
	<h2 class="center">Why Most SWOT Analyses Stink</h2>

	<p class="center">by Rich Horwath</p>

	<div class="columns">
		
		<p><em>The difference between success and failure is doing a thing nearly right and doing a thing exactly right.</em></p>
		
		<div class="right">
			&#8212;Edward Simmons<br>
			American Painter
		</div>
		
		<p>A recent study showed that US children are continuing to fall behind in the global education rankings, placing 19th in the world in the latest report. Let&#8217;s hope those same researchers don&#8217;t get around to ranking our SWOT analyses efforts any time soon.</p>
		
		
		<p>Like most things utterly familiar, SWOT analysis has been taken for granted to the point that it has become perhaps the most misused tool in the manager&#8217;s box. A good SWOT analysis is one of the cornerstones of good strategy. A half-baked SWOT analysis (and most of them are) prevents businesses from realizing their full potential and weds them to mediocrity. Let&#8217;s look at what it takes to move from doing a SWOT analysis nearly right to doing it exactly right&#8212;a difference which can jumpstart your strategy.</p>
		
		<h3>Behind the Acronym</h3>
		
		<p>SWOT analysis is one of the most effective and universally used models in business. Developed in 1971, it provides a simple yet comprehensive method for examining the strategic fit between a firm&#8217;s internal capabilities (strengths and weaknesses) and external possibilities (opportunities and threats). The acronym SWOT stands for Strengths, Weaknesses, Opportunities, Threats.</p>
		
		<p>Strengths and weaknesses are internal factors we generally have <em>control&nbsp;</em> over. Opportunities and threats are external factors that we can <em>influence&nbsp;</em>. The SWOT analysis model helps us answer two fundamental questions:</p>
		
		<ol>
			<li> What do we have (strengths and weaknesses)?</li>
			<li>What might we do (opportunities and threats)?</li>
		</ol>
		
		<p>Below are the criteria and guidelines for each of the four categories. The word &#8220;group&#8221; is used as a general representation for a company, division, business unit, brand, etc. depending on the appropriate application.</p>
		
		
		<p><strong><u>Strengths:</u></strong> Strengths are those factors that make a group more competitive than its peers. Strengths are what the group has a distinctive advantage at doing or what resources it has that are superior to its peers. Strengths are in effect resources or capabilities that the group holds that can be used effectively to achieve its performance objectives.</p>
		
		<p><strong><u>Weaknesses:</u></strong> Weaknesses are limitations, faults or defects within the group that will keep it from achieving its objectives. It is what a group does poorly and where it has inferior capabilities or resources as compared to its peers.</p>
		
		<p><strong><u>Opportunities:</u></strong> Opportunities include any favorable current or prospective situation in the group&#8217;s environment, such as a trend, change or overlooked need that supports the demand for a product or service and permits the group to enhance its competitive position.</p>
		
		<p><strong><u>Threats:</u></strong> A threat includes any unfavorable situation, trend or impending change in a group&#8217;s environment that is currently or potentially damaging or threatening to its ability to compete. It may be a barrier, constraint or anything that might inflict problems, damage, harm or injury to the group.</p>
		
		<p>In general, strengths and weaknesses (internal environment) are made up of factors over which the group has greater relative control. These factors may include the following:</p>
		
		<ul>
			<li>Skill sets</li>
			<li>Resources (people, money, time)</li>
			<li>Knowledge base</li>
			<li>Processes and systems, including operational and customer-facing</li
			><li>Staffing practices</li>
			<li>Brand</li>
			<li>Values</li>
			<li>Culture</li>
		</ul>
		
		<p>Opportunities and threats (external environment) are made up of those factors over which the organization has influence but not control. These factors include:</p>
		
		<ul>
			<li>Overall demand</li>
			<li>Competitor activity</li>
			<li>Market saturation</li>
			<li>Government policies</li>
			<li>Economic conditions</li>
			<li>Social, cultural, and ethical developments</li>
			<li>Technological developments</li>
		</ul>
		
		<h3>Five SWOT Killers</h3>
		
		<p>As with any business model, the reason a SWOT analysis is performed is to generate insights about the business and drive planning efforts. There are five SWOT killers that we need to be aware of to prevent our efforts from being an exercise in futility:</p>
		
		<p><strong>1. The Laundry List</strong><br>
		When listing the individual elements under each category, careful thought should be given to the importance of each item. A laundry list of 7-10 factors for each category is fine in the first draft. However, only the elements with significant impact on the business should be recorded in the final analysis (approximately 3-5). Marshaling the mental discipline to create a tight SWOT analysis enables you to move into planning mode with greater clarity and focus&#8212;two keys to strong strategy.</p>
			
		<p><strong>2. Generalities</strong><br>
		There is a fine line between the factors in the SWOT analysis being brief and being meaningless. How many times have you seen a SWOT analysis with &#8220;quality&#8221; listed under the strengths or weaknesses column? That&#8217;s about as useful as putting down the word &#8220;the.&#8221; The factors listed need to be specific enough so that someone reading the analysis without the creator sitting next to them can understand to a reasonable degree what is meant by the factor. Recording &#8220;manufacturing line breakdowns&#8221; rather than &#8220;quality&#8221; as a weakness is much more helpful because it alerts the reader to the specific cause of the issue.</p>
		
		<p><strong>3. Special &#8220;Effects&#8221;</strong><br>
		Another common mistake is listing the effect rather than the cause of a strength or weakness. An example of a strength often listed is &#8220;#1 market share.&#8221; What would be much more helpful is to list the cause of that #1 market share&#8212;i.e., consultative selling skills of sales reps, 3:1 advertising spend ratio versus the competition, etc. Listing the cause also plays another important role by allowing managers to more easily identify and share best practices among the group. Knowing what&#8217;s driving market share leadership can be usefully applied to other groups within the organization.</p>
		
		<p><strong>4. Mistaking Influence for Control</strong><br>
		Despite the clearly defined lines of strengths/weaknesses being internal and controllable and opportunities/threats being external and influenced, factors are often mistakenly placed in the model. The rule of thumb is if you can allocate your resources to a factor and control it, it is a strength/weakness. If your resource allocation can influence but not necessarily control the factor, it is an opportunity/threat.</p>
			
		<p><strong>5. Not Quantifying Opportunities & Threats</strong><br>
		In physics, the mass of an object is a relative quantity. In the world of SWOT analysis, the size of opportunities and threats are relative quantities as well. Managers of two different brands both might list the opportunity of &#8220;large Medicare population&#8221; for their biotech product. However, &#8220;large&#8221; for one manager might mean a $2 million opportunity while &#8220;large&#8221; for the other manager might mean a $20 million opportunity. If we haven&#8217;t quantified the opportunity in the SWOT analysis then how will we know? More importantly, how will any of the other key players in the organization know? Quantifying opportunities and threats allows you to more confidently allocate your limited resources to those that will provide the greatest return on investment. While you won&#8217;t always have all of the necessary data to make an exact quantification of the opportunity/threat, go ahead and give a rough percentage or ratio (i.e., competitor ad spending is 2:1 in this market segment).</p>
		
		<h3>Answering SWOT&#8217;s &#8220;So What&#8217;s?&#8221;</h3>
		
		<p>Once you've completed the SWOT Analysis, the typical response is "So what now? What do I actually do with it?" I've created a sequence of three steps to go from SWOT Analysis to strategy. Step 1 is the SWOT Analysis. Step 2 is to use the Opportunity & Threat Matrices to prioritize the opportunities and threats based on probability and impact. Step 3 consists of SWOT Alignment where you align strengths and weaknesses with opportunities and threats to develop potential strategies. SWOT can be a powerful tool when used correctly and can be a time sucking, snooze-fest when used incorrectly. How well does your group use it?</p>


	</div>
</article>