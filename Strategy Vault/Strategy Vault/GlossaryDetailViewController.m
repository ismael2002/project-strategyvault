//
//  GlossaryDetailViewController.m
//  Strategy Vault
//
//  Created by Chris Anderson on 9/4/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import "GlossaryDetailViewController.h"

@interface GlossaryDetailViewController ()

@end

@implementation GlossaryDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *content = [self.item objectForKey:@"description"];
    NSString *title = [self.item objectForKey:@"title"];
    
    self.textView.text  = content;
    self.titleLabel.text = title;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self.textView setFont:[ self.textView.font fontWithSize:22] ];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
