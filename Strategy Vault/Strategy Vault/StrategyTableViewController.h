//
//  StrategyTableViewController.h
//  Strategy Vault
//
//  Created by Chris Anderson on 9/8/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StrategyTableViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *discovery;
@property (strong, nonatomic) IBOutlet UIButton *strategicThinking;
@property (strong, nonatomic) IBOutlet UIButton *strategicPlanning;
@property (strong, nonatomic) IBOutlet UIButton *strategyRollout;
@property (strong, nonatomic) IBOutlet UIButton *strategyTuneUp;

- (IBAction)discoveryList:(id)sender;
- (IBAction)strategicThinkingList:(id)sender;
- (IBAction)strategicPlanningList:(id)sender;
- (IBAction)strategyRolloutList:(id)sender;
- (IBAction)strategyTuneUpList:(id)sender;

- (void)saveBehavior:(NSDictionary *)behavior atIndex:(NSIndexPath *)indexPath;

@end
