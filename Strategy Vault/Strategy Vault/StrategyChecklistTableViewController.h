//
//  StrategyChecklistTableViewController.h
//  Strategy Vault
//
//  Created by Chris Anderson on 9/8/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StrategyTableViewController.h"

@interface StrategyChecklistTableViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@property NSDictionary *behaviors;
@property NSInteger section;
@property StrategyTableViewController *parent;

@end