//
//  InsightViewController.h
//  Strategy Vault
//
//  Created by Ismael Zavala on 10/20/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "BaseViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "InsightTableViewController.h"

@interface InsightViewController : BaseViewController <AVAudioRecorderDelegate, AVAudioPlayerDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *titleField;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *stopButton;
@property (weak, nonatomic) IBOutlet UIButton *recordButton;

- (IBAction)saveButtonTapped:(id)sender;


@property (weak, nonatomic) IBOutlet UIBarButtonItem *savebutton;


@property NSMutableDictionary *insight;
@property NSInteger index;
@property InsightTableViewController *parent;

@property (weak, nonatomic) IBOutlet UIPickerView *picker;

@end
