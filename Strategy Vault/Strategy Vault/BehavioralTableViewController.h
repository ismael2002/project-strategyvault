//
//  BehavioralTableViewController.h
//  Strategy Vault
//
//  Created by Chris Anderson on 9/4/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BehavioralTableViewController : UITableViewController <NSCoding, NSObject>

@property (nonatomic, strong) NSMutableArray *checked;

@property (nonatomic, strong) NSString *test;

- (void)saveBehavior:(NSDictionary *)behavior atIndex:(NSIndexPath *)indexPath;



@end

