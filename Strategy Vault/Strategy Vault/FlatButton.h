//
//  FlatButton.h
//  Strategy Vault
//
//  Created by Chris Anderson on 8/22/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlatButton : UIButton

- (void)sizeToText;

@end
