//
//  ThoughtViewController.m
//  Strategy Vault
//
//  Created by Chris Anderson on 9/2/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import "ThoughtViewController.h"

@interface ThoughtViewController ()

@end

@implementation ThoughtViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSArray *json = [Utilities jsonArrayFromFile:@"thoughts_of_the_day"];
 
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    
    NSInteger day = [components day];
    NSInteger month = [components month];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    NSString *monthName = [[df monthSymbols] objectAtIndex:(month-1)];
    self.day.text = $str(@"%@ %ld", monthName, (long)day);
    UITextView *quoteView = [[UITextView alloc] init];
    
    
    
    for (NSDictionary *quote in json) {
        if ([[quote objectForKey:@"month"] integerValue] == month
            && [[quote objectForKey:@"day"] integerValue] == day) {
            
            NSString *htmlContent = [quote objectForKey:@"content"];
            NSString *htmlQuote = [quote objectForKey:@"quote"];
            BOOL emptyQuote = YES;
            
            UILabel *authorLable = [[UILabel alloc] init];
            authorLable.text = [quote objectForKey:@"auth"];
            [authorLable sizeToFit];
            
            
            //if there is a quote in Json
            if (![htmlQuote  isEqual: @""]) {
                
                emptyQuote = NO;
                
                NSAttributedString *quote = [[NSAttributedString alloc] initWithData:[htmlQuote dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
                
                CGRect quoteViewFrame = CGRectMake(20, 0, 280, 300);
                quoteView.frame = quoteViewFrame;
                quoteView.returnKeyType = UIReturnKeyDone;
                [self.scrollView addSubview:quoteView];
                quoteView.attributedText = quote;
                [quoteView setFont:[quoteView.font fontWithSize:20]];
                [quoteView sizeToFit];
                [quoteView setBackgroundColor:[UIColor clearColor]];
                
//                // sets up the size and position of the image to wrap the text around it
                //UIBezierPath* exclusionPath = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, 40, 40)];
                //UIBezierPath* secondExclusionPath = [UIBezierPath bezierPathWithRect:CGRectMake(quoteView.frame.size.width , quoteView.frame.size.height - 40 , 40, 40)];
               //quoteView.textContainer.exclusionPaths = @[exclusionPath, secondExclusionPath];
                
                //resizes the frame to make sure that because of the exclusion path added,
                //it didn't create a new line, changing the frame
                [quoteView sizeToFit];
                
                //creates exclusion paths for text to wrap around images
                //exclusionPath = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, 50, 40)];
                //secondExclusionPath = [UIBezierPath bezierPathWithRect:CGRectMake(quoteView.frame.size.width - 55, quoteView.frame.size.height - 20 , 40, 40)];
                //quoteView.textContainer.exclusionPaths = @[exclusionPath, secondExclusionPath];
                
                
                //places opening quotes into the spot of the exclusion path
//                UIImageView *openingQuotesImageHolder = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 40, 40)];
//                UIImage *openingQuotesImage = [UIImage imageNamed:@"quotes-568.png"];
//                openingQuotesImageHolder.image = openingQuotesImage;
//                [quoteView addSubview:openingQuotesImageHolder];
                
                //sets images to the empty spot created by the second exclusion path
//                UIImageView *closingQuotesImageHolder = [[UIImageView alloc] initWithFrame:CGRectMake (quoteView.frame.size.width - 55, quoteView.frame.size.height - 40 , 40, 40)];
//                UIImage *image = [UIImage imageNamed:@"closingquotes-568.png"];
//                closingQuotesImageHolder.image = image;
//                [quoteView addSubview:closingQuotesImageHolder];
//                [quoteView sizeToFit];
            }
            
            //if there is something in the content field
            if (![htmlContent  isEqual: @""]) {
                
                NSAttributedString *content = [[NSAttributedString alloc] initWithData:[htmlContent dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
                
                if (emptyQuote) {
             
                    CGRect textViewFrame = CGRectMake(20, 0, 280, 300);
                    UITextView *textView = [[UITextView alloc] initWithFrame:textViewFrame];
                    textView.returnKeyType = UIReturnKeyDone; // make it not editable
                    [self.scrollView addSubview:textView];
                    textView.attributedText = content;
                    [textView setFont:[textView.font fontWithSize:20]];
                    [textView sizeToFit];
                    [textView setBackgroundColor:[UIColor clearColor]];
                    
                    NSInteger textViewHeight = textView.frame.size.height;
                    NSInteger textViewOriginY = textView.frame.origin.y;
                    
                    //place author lable below the content when there is no quote
                    //authorLabel frame variables
                    double authorLabelYPosition = textViewHeight + textViewOriginY + 20;
                    
                    [self placeAuthorLabel:textView authorLablePosition:&authorLabelYPosition authorLabel:authorLable];
                }
                else
                {
                    //size of the quote text view
                    NSInteger quoteViewHeight = quoteView.frame.size.height;
                    NSInteger quoteViewOriginY = quoteView.frame.origin.y;
                    
                    // where the extraText should be, which is after the quote
                    NSInteger textViewPosition = quoteViewHeight + quoteViewOriginY + 20;
                    
                    CGRect textViewFrame = CGRectMake(25, textViewPosition, 280, 300);
                    UITextView *textView = [[UITextView alloc] initWithFrame:textViewFrame];
                    textView.returnKeyType = UIReturnKeyDone;
                    [self.scrollView addSubview:textView];
                    textView.attributedText = content;
                    [textView setFont:[textView.font fontWithSize:20]];
                    [textView sizeToFit];
                    [textView setBackgroundColor:[UIColor clearColor]];
                    
                    //if there is a quote, place author lable after textview
                    NSInteger textViewHeight = textView.frame.size.height;
                    NSInteger textViewOriginY = textView.frame.origin.y;
                    double authorLabelYPosition = textViewHeight + textViewOriginY + 20;
                    
                    [self placeAuthorLabel:textView authorLablePosition:&authorLabelYPosition authorLabel:authorLable];
                }
            }
            else
            {
                // if there is no text after the quote, author label gets placed after the quote
                
                //quoteView Variables
                NSInteger quoteViewOriginY = quoteView.frame.origin.y;
                NSInteger quoteViewHeight = quoteView.frame.size.height;
                
                //authorLabel frame variables
                double authorLabelYPosition = quoteViewOriginY + quoteViewHeight + 20;
                
                //create author label on correct position
                [self placeAuthorLabel:quoteView authorLablePosition:&authorLabelYPosition authorLabel:authorLable];
            }
            return;
        }
    }
}

-(void) placeAuthorLabel: (UITextView *)view authorLablePosition: (double *)position authorLabel:(UILabel *)lable
{
    CGRect authorFrame = CGRectMake(25, *position, lable.frame.size.width, lable.frame.size.height);
    lable.frame = authorFrame;
    [lable setFont:[lable.font fontWithSize:20]];
    [lable sizeToFit];
    [self.scrollView addSubview:lable];
    
    //adjust ScrollView Height after everything is placed
    double lableHeight = lable.frame.size.height;
    double scrollViewHeight = (*position + lableHeight + 20);
    
    self.scrollView.contentSize = CGSizeMake(320, scrollViewHeight);
    self.scrollView.scrollEnabled = YES;
}


- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillAppear:animated];
    [self.navigationController setToolbarHidden:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
