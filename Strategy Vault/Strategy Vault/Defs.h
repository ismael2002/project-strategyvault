//
//  Defs.h
//  Strategy Vault
//
//  Created by Chris Anderson on 8/28/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#define kUrl @"http://strategicthinking.org"
#define kMediaUrl @"http://www.strategyskills.com/mobile"


/// Logging
#define debug(...) NSLog(@"%s %@", __PRETTY_FUNCTION__, [NSString stringWithFormat:__VA_ARGS__])

/// Color handling
/// Usage: UIColor *green = $rgb(0,255,0);
/// Returns: UIColor
#define $rgb(red, green, blue)  [UIColor colorWithRed:(red)/255.0 green:(green)/255.0 blue:(blue)/255.0 alpha:1]
#define $hex(rgbValue)          [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

/// String manipulation
/// Usage: NSString *hello = $str(@"Hello %@", @"World");
/// Returns: NSString
#define $str(...)           [NSString stringWithFormat:__VA_ARGS__]
#define $mstr(...)          [NSMutableString stringWithFormat:__VA_ARGS__]
#define $str_from_bool(bool)   (bool ? @"YES" : @"NO")
#define $blank_if_empty(string)     ((string == nil || string.length == 0) ? @"" : string)

/// Generating NSNumber from different types
/// Usage: NSNumber *n = $bool(YES);
/// Returns: NSNumber
#define $bool(val)      [NSNumber numberWithBool:(val)]
#define $char(val)      [NSNumber numberWithChar:(val)]
#define $double(val)    [NSNumber numberWithDouble:(val)]
#define $float(val)     [NSNumber numberWithFloat:(val)]
#define $int(val)       [NSNumber numberWithInt:(val)]
#define $integer(val)   [NSNumber numberWithInteger:(val)]
#define $long(val)      [NSNumber numberWithLong:(val)]
#define $longlong(val)  [NSNumber numberWithLongLong:(val)]
#define $short(val)     [NSNumber numberWithShort:(val)]
#define $uchar(val)     [NSNumber numberWithUnsignedChar:(val)]
#define $uint(val)      [NSNumber numberWithUnsignedInt:(val)]
#define $uinteger(val)  [NSNumber numberWithUnsignedInteger:(val)]
#define $ulong(val)     [NSNumber numberWithUnsignedLong:(val)]
#define $ulonglong(val) [NSNumber numberWithUnsignedLongLong:(val)]
#define $ushort(val)    [NSNumber numberWithUnsignedShort:(val)]
#define $round(val)     (floorf(val * 100) / 100)

/// Conditionals
/// Usage: if ($empty(string)) { ... }
/// Returns: Boolean
#define $safe(obj)          ((NSNull *)(obj) == [NSNull null] ? nil : (obj))
#define $equal(a,b)         [(a) isEqual:(b)]
#define $empty(string)      (string.length == 0)
#define $ifnil(obj1,obj2)   (obj1 == nil ? obj2 : obj1)

/// iOS version identification
/// Usage: $ios_equal_to(@"5.0")
/// Returns: NSString
#define $ios_equal_to(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define $ios_greater_than(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define $ios_less_than(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

/// iOS device identification
/// Usage: if ($is_iPad) { ... }
/// Returns: Boolean
#define $is_iPad            (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define $is_iPhone          (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define $is_retina          ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] >= 2)
#define $has_multitasking   ([[UIDevice currentDevice] respondsToSelector:@selector(isMultitaskingSupported)] && [[UIDevice currentDevice] isMultitaskingSupported])

/// iOS device versioning
/// Usage: cell.text = $version;
/// Returns: NSString
#define $build              [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]
#define $version            [NSString stringWithFormat:@"Ver %@ Build %@", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"], [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];

/// Alert view
/// Usage: $alert(@"Title", @"Message");
#define $alert(title, msg) [[[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show]

/// Rectangle
/// Usage: view.frame = $rect(10.0f, 10.0f, 10.0f, 10.0f);
/// Returns: CGRect
#define $rect(x, y, width, height)          CGRectMake(x, y, width, height)
#define $rect_with_height(rect, height)     CGRectMake((rect).origin.x, (rect).origin.y, (rect).size.width, (height))
#define $rect_with_width(rect, width)      CGRectMake((rect).origin.x, (rect).origin.y, (width), (rect).size.height)
#define $rect_with_x(rect, height)          CGRectMake((x), (rect).origin.y, (rect).size.width, (rect).size.height)
#define $rect_with_y(rect, width)           CGRectMake((rect).origin.x, (y), (rect).size.width, (rect).size.height)


/// Math helpers
/// Usage: $deg_to_radians(180);
/// Returns: float
#define $deg_to_radians(degrees)    ((degrees) * M_PI / 180.0)
#define $rad_to_degrees(radians)    ((radians) * 180.0 / M_PI)

