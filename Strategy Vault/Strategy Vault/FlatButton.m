//
//  FlatButton.m
//  Strategy Vault
//
//  Created by Chris Anderson on 8/22/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import "FlatButton.h"

@implementation FlatButton

+ (instancetype)button
{
    return [self buttonWithType:UIButtonTypeCustom];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

#pragma mark - Instance methods

- (UIEdgeInsets)titleEdgeInsets
{
    return UIEdgeInsetsMake(4.f,
                            4.f,
                            4.f,
                            4.f);
}

- (CGSize)intrinsicContentSize
{
    CGSize s = [super intrinsicContentSize];
    
    return CGSizeMake(s.width + self.titleEdgeInsets.left + self.titleEdgeInsets.right,
                      s.height + self.titleEdgeInsets.top + self.titleEdgeInsets.bottom);
    
}

- (void)sizeToText
{
    CGRect frame = self.frame;
    
    // Measures string
    CGSize stringSize = [self.titleLabel.text sizeWithFont:self.titleLabel.font];
    frame.size.width = stringSize.width + 20.0f;
    
    [self setFrame:frame];
}

#pragma mark - Private instance methods

- (void)setup
{
    self.layer.cornerRadius = 4.f;
    self.clipsToBounds = YES;
    [self setTitleColor:[UIColor whiteColor]
               forState:UIControlStateNormal];
    self.titleLabel.font = [UIFont fontWithName:@"Avenir-Medium"
                                           size:16];
}

@end

