//
//  BehavioralTableViewController.m
//  Strategy Vault
//
//  Created by Chris Anderson on 9/4/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import "BehavioralTableViewController.h"
#import "BehaviorChecklistTableViewController.h"

@interface BehavioralTableViewController ()

@end

@implementation BehavioralTableViewController
{
    NSMutableArray *behaviors;
    NSString *isChecked;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSArray *behaviorsStable = [Utilities jsonArrayFromFile:@"behavioral"];
    behaviors = [behaviorsStable mutableCopy];
    
    for (int i = 0; i < [behaviorsStable count]; i++) {
        NSDictionary *dic = behaviorsStable[i];
        behaviors[i] = [dic mutableCopy];
        
        NSMutableArray *mutableArray = [[NSMutableArray alloc] init];
        for (NSDictionary *item in dic[@"items"]) {
            [mutableArray addObject:[item mutableCopy]];
        }
        behaviors[i][@"items"] = mutableArray;
    }

    for (NSInteger x = 0; x < [behaviors count]; x++)
    {
        [self.checked insertObject:@"false" atIndex:x];
    }
    
    [self.tableView setBackgroundView:
     [[UIImageView alloc] initWithImage:
      [UIImage imageNamed:@"background-slice-"]]];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillAppear:animated];
    [self.navigationController setToolbarHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)saveBehavior:(NSDictionary *)behavior atIndex:(NSIndexPath *)indexPath
{
    behaviors[indexPath.section][@"items"][indexPath.item] = behavior;
    
    [Utilities jsonToFile:behaviors filename:@"behavioral"];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [behaviors count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"behavioralCell" forIndexPath:indexPath];
    
    UILabel *textLabel = (UILabel *) [cell viewWithTag: 102]; 
    
    textLabel.text = [[behaviors objectAtIndex:indexPath.row] objectForKey:@"title"];
    
    
    isChecked = self.checked [indexPath.row];
    
    UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"steel-bar-.png"]];
    
    cell.backgroundView = backgroundView;
    
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.checked insertObject:@"true" atIndex:indexPath.row];
    
    self.test = @"test worked";
    
    [self performSegueWithIdentifier:@"checklist" sender:indexPath];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = sender;
    BehaviorChecklistTableViewController *vc = segue.destinationViewController;
    vc.section = indexPath.row;
    vc.parent = self;
    vc.behaviors = [behaviors objectAtIndex:indexPath.row];
}


@end
