//
//  ThoughtViewController.h
//  Strategy Vault
//
//  Created by Chris Anderson on 9/2/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThoughtViewController : UIViewController 

@property (weak, nonatomic) IBOutlet UILabel *day;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property(copy, nonatomic) NSArray *exclusionPaths;


@end
