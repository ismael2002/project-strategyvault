//
//  LoginViewController.h
//  Strategy Vault
//
//  Created by Chris Anderson on 8/28/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface LoginViewController : BaseViewController

@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *code;

@property (weak, nonatomic) IBOutlet UIButton *submit;
@property (weak, nonatomic) IBOutlet UIButton *invite;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollLogin;

- (IBAction)submit:(id)sender;
- (IBAction)invite:(id)sender;

@end
