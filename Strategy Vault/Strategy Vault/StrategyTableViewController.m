//
//  StrategyTableViewController.m
//  Strategy Vault
//
//  Created by Chris Anderson on 9/8/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import "StrategyTableViewController.h"
#import "StrategyChecklistTableViewController.h"

@interface StrategyTableViewController ()

@end

@implementation StrategyTableViewController
{
    NSMutableArray *strategies;
    
    NSInteger row;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSArray *behaviorsStable = [Utilities jsonArrayFromFile:@"strategies"];
    strategies = [behaviorsStable mutableCopy];
    
    for (int i = 0; i < [behaviorsStable count]; i++) {
        NSDictionary *dic = behaviorsStable[i];
        strategies[i] = [dic mutableCopy];
        
        NSMutableArray *mutableArray = [[NSMutableArray alloc] init];
        for (NSDictionary *item in dic[@"items"]) {
            [mutableArray addObject:[item mutableCopy]];
        }
        strategies[i][@"items"] = mutableArray;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillAppear:animated];
    [self.navigationController setToolbarHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)saveBehavior:(NSDictionary *)behavior atIndex:(NSIndexPath *)indexPath
{
    strategies[indexPath.section][@"items"][indexPath.item] = behavior;
    
    [Utilities jsonToFile:strategies filename:@"strategies"];
}

- (IBAction)discoveryList:(id)sender
{
    row = 0;
    
    [self performSegueWithIdentifier:@"checklist" sender:self];

    NSLog(@"DISCOVERY LIST");
}

- (IBAction)strategicThinkingList:(id)sender
{
    row = 1;
    
    [self performSegueWithIdentifier:@"checklist" sender:self];
    
    NSLog(@"strategic Thinking List");
    
}

- (IBAction)strategicPlanningList:(id)sender
{
    row = 2;
    
    [self performSegueWithIdentifier:@"checklist" sender:self];
    
    NSLog(@"strategic Planning List");
}

- (IBAction)strategyRolloutList:(id)sender
{
    row = 3;
    
    [self performSegueWithIdentifier:@"checklist" sender:self];
    
    NSLog(@"strategy Rollout List");
}

- (IBAction)strategyTuneUpList:(id)sender
{
    row = 4;
    
    [self performSegueWithIdentifier:@"checklist" sender:self];
    
    NSLog(@"strategy Tune Up List");
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    StrategyChecklistTableViewController *vc = segue.destinationViewController;
    vc.section = row;
    vc.parent = self;
    vc.behaviors = [strategies objectAtIndex:row];
}


@end
