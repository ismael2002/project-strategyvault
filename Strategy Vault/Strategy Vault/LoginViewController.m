//
//  LoginViewController.m
//  Strategy Vault
//
//  Created by Chris Anderson on 8/28/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import "LoginViewController.h"
#import <Parse/Parse.h>

@interface LoginViewController ()

@end

@implementation LoginViewController
{
    NSUserDefaults *defaults;
    NSString *userEmail;
    NSString *userCode;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    defaults = [NSUserDefaults standardUserDefaults];
    
    userEmail = [defaults objectForKey:@"email"];
    userCode  = [defaults objectForKey:@"code"];
    
    NSLog(@"THE USER EMAIL IS %@", userEmail);
    

    @try {
        if (! [userEmail isEqualToString:@""] && ![userCode isEqualToString:@""]) {
            PFQuery *query = [PFQuery queryWithClassName:@"Person"];
            [query setLimit: 1000];
            [query whereKey:@"username" equalTo:userEmail];
            [query whereKey:@"key" equalTo:userCode];
            
            //[query setLimit: 1000];
            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                if (!error && ![userCode isEqualToString:@""] && objects.count != 0) {
                    
                    [self login];
                    
                } else {
                    NSLog(@"THERE IS NO PREVIOUS LOG IN");
                }
            }];
        }
    }
    @catch (NSException *exception) {
        
    }
  
    [super viewDidLoad];

    //adjusting view for iphone 4
    self.navigationController.navigationBar.translucent = NO;
    CGRect fullScreenRect = [[UIScreen mainScreen] applicationFrame];
    self.view.bounds = CGRectMake(0, 66, fullScreenRect.size.width, fullScreenRect.size.height - 66);
    [self.view addSubview:self.scrollLogin];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
    self.email.leftView = paddingView;
    self.email.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
    self.code.leftView = paddingView2;
    self.code.leftViewMode = UITextFieldViewModeAlways;
    
    
    self.code.secureTextEntry = YES;
    
    
    UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"background-slice-"]];
    self.view.backgroundColor = background;
    
    [self.email setKeyboardType:UIKeyboardTypeEmailAddress];
    [self.code setReturnKeyType:UIReturnKeyDone];
    //self.email.autocorrectionType = UITextAutocorrectionType.No;
    [self.email setAutocorrectionType:UITextAutocorrectionTypeNo];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    

}

- (IBAction)submit:(id)sender
{
    userEmail = self.email.text;
    userCode= self.code.text;
    
    userEmail = userEmail.lowercaseString;
    
    [self.submit setEnabled:NO];
    
    
    PFQuery *query = [PFQuery queryWithClassName:@"Person"];
    [query setLimit: 1000];
    [query whereKey:@"username" equalTo:userEmail];
    [query whereKey:@"key" equalTo:userCode];
    
    //[query setLimit: 1000];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error && ![userCode isEqualToString:@""] && objects.count != 0) {
            
            defaults = [NSUserDefaults standardUserDefaults];
            
            [defaults setObject:userEmail forKey:@"email"];
            [defaults setObject:userCode forKey:@"code"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"Data saved");
            
            userEmail = [defaults objectForKey:@"email"];
            NSLog(@"The user email is %@", userEmail);
            
            [self login];
            
        } else {
            // Log details of the failure
            $alert(@"Error", @"Unable to login with credentials provided");
            [self.submit setEnabled:YES];
        }
    }];
    
}

- (IBAction)invite:(id)sender
{
    
}

- (void)login
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"home"];
    [self presentViewController:viewController animated:NO completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField
{
    if (theTextField == self.email)
    {
        [theTextField resignFirstResponder];
        [self.code becomeFirstResponder];
    }
    
    else if (theTextField == self.code)
    {
        [self.submit sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
