//
//  LibraryTableViewController.m
//  Strategy Vault
//
//  Created by Chris Anderson on 9/2/14.
//  Copyright (c) 2014 Bolder Image. All rights reserved.
//

#import "LibraryTableViewController.h"
#import "LibraryItemsTableViewController.h"

@interface LibraryTableViewController ()

@end

@implementation LibraryTableViewController
{
    NSArray *articles;
    NSArray *audio;
    NSArray *video;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    articles = [Utilities jsonArrayFromFile:@"articles"];
    audio = [Utilities jsonArrayFromFile:@"audio"];
    video = [Utilities jsonArrayFromFile:@"video"];
    
    [self.tableView setBackgroundView:
     [[UIImageView alloc] initWithImage:
      [UIImage imageNamed:@"background-slice-"]]];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillAppear:animated];
    [self.navigationController setToolbarHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView
                             dequeueReusableCellWithIdentifier:@"libraryCategories"];
    
    UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"steel-bar-.png"]];

    
    cell.backgroundView = backgroundView;
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    
    UILabel *label = (UILabel *) [cell viewWithTag: 1000];
    
    if (indexPath.row == 0)
    {
        label.text = @"Articles";
        cell.imageView.image = [UIImage imageNamed:@"articles-icon-.png"];
        
    } else if ( indexPath.row == 1) {
        label.text = @"Videos";
        cell.imageView.image = [UIImage imageNamed:@"video-icon-.png"];
    } else if ( indexPath.row == 2){
        label.text = @"Podcasts";
        cell.imageView.image = [UIImage imageNamed:@"mp3-icon-.png"];
    }
    
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    [self performSegueWithIdentifier:@"LibraryItems" sender:indexPath];
}




#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = sender;
    LibraryItemsTableViewController *vc = segue.destinationViewController;
    if (indexPath.row == 0) {
        vc.items = articles;
    }
    if (indexPath.row == 1) {
        vc.items = video;
    }
    if (indexPath.row == 2) {
        vc.items = audio;
    }
}




@end
